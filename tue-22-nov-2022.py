names = ["quratulain", "noorulain", "taybain", "'arifain"]
# let's print "and" after each names in list
# using for-loop
for name in names:
    print(name, "and",end=" ")
print("mother name is masoma")

# using join function
a = " and ".join(names)
print(a)
# similarly
b = ",".join(names)
print(b)
