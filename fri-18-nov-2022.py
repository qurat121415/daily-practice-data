############### TIME MODULE ################
import time
initial = time.time()
print(initial)

initial1 = time.time()
k = 0
while k < 4:
    print("this is quratulain")
    k = k+1
print(time.time()-initial1, "seconds")

initial2 = time.time()
for i in range(5):
    print("this is quratulain")
print(time.time()-initial2, "seconds")


############# Python time module functions #############

# 1. time.time() method
import time
seconds = time.time()
print("Current time in seconds since epoch =", seconds)
# epoch is basically the starting point of time from which the lapse of time is to be measured.

# 2. time.sleep() method
import time
print("This is Quratulain")
time.sleep(1.2)
print("This is Huma")
time.sleep(3.2)
print("We are besties")

# 3. time.localtime() method
import time
local_time = time.localtime()
print("Time:", local_time)
print("Current year:", local_time.tm_year)
print("Current hour:", local_time.tm_hour)
print("Current minute:", local_time.tm_min)
print("Current second:", local_time.tm_sec)

# 4. time.ctime() method
from time import time, ctime
current_time = time()
print(current_time) # epoch
et = ctime(current_time)
print("local time: ",et)

# 5. time.mktime() method
# The time.mktime() method is the inverse of time.localtime() method.
import time
local_time = time.localtime()
print(local_time)
sec = time.mktime(local_time)
print(sec)

# 6. time.asctime() method
# The time.asctime() method takes tuples of struct_time class as an argument and
# it returns a string representing the time input from the struct_time class tuples
import time
local_time = (time.time())
print(local_time)
print(type(local_time))

local_time = time.localtime(time.time())
print(local_time)
print(type(local_time))

local_time = time.asctime(time.localtime(time.time()))
print(local_time)
print(type(local_time))
