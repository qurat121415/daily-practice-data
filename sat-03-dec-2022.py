# finding length of list
# method 1, naive method
list = [1, 2, 3, 4, 5, 8, 9]
counter = 0
for num in list:
    counter = counter + 1
print("the list is:", list)
print("length of list by using naive method is:", counter)

# method 2, using len(list) method
list2 = [1, 2, 3, 5, 6, 8, 9]
print("the list is:", list2)
print("length of list by using len() method is:", len(list2))

# method 3, using length_hint
from operator import length_hint
list3 = [9, 8, 6, 5, 3, 2, 1]
print("the list is:", list3)
print("length of list by using length_hint method is:", length_hint(list3))

# time analysis
list = [1, 2, 3, 4, 5, 8, 9]
from operator import length_hint
import time
print("the list is:", list)
start_time_naive = time.time()
counter = 0
for num in list:
    counter = counter + 1
end_time_naive = time.time() - start_time_naive

start_time_len = time.time()
list_len = len(list)
end_time_len = time.time() - start_time_len

start_time_len_hint = time.time()
len_hint = length_hint(list)
end_time_len_hint = time.time() - start_time_len_hint
print("time taken by using naive method is:", end_time_naive)
print("time taken by using len() method is:", end_time_len)
print("time taken by using length_hint method is:", end_time_len_hint)

# method 4, sum method
list4 = [1, 2, 3, 4, 5, 6, 7]
sum = sum(1 for i in list4)
print("length of list by using sum method is:", sum)
