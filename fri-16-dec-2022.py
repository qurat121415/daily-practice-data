# direct printing
print("Hello world")
# indirect method
# creating variable
var = "Hello world"
print(var)

# work with str, int, float
var1 = 4
var2 = 34.7
print(var1+var2) #addable
var3 = "54"
var4 = "32"
print(var3+var4) #not addable
# we can convert str into int
print(int(var3)+int(var4)) #addable
print(var1+var4) #ERROR
