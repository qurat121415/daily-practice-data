# way of defining function that we are familiar with
def names(a, b, c, d):
    print("names are:\n", a, b, c, d)

names("qurat", "huma", "asia", "ayesha")

# In this method we cannot add further names as it will give error
# so for adding multiple names we can use *args
list = ["qurat", "huma", "asia", "ayesha"]
def funargs(*args):
    print(args)
funargs(*list)

# in args method list converts into tuple
list = ["qurat", "huma", "asia", "ayesha"]
def func(*tuple):
    print(type(tuple))
    print(tuple)
func(*list)

#  even if we add names now it will not give an error
list = ["qurat", "huma", "asia", "ayesha", "The programmer"]
def func(*args):
    print(args)
func(*list)

# let's use for_loop in it
list = ["qurat", "huma", "asia", "ayesha", "The programmer"]
def func(*args):
    for names in args:
        print(names)
func(*list)


# we can also use normal argument along with *args
list = ["qurat", "huma", "asia", "ayesha", "The programmer"]
normal = "i am a normal argument and the names of students are"
def func(normal, *args):
    print(normal)
    for names in args:
        print(names)
func(normal, *list)

# we can give any name to args and kwargs but putting (*) sign is important
# giving sequence is also important
# (1)normal (2)*args (3)**kwargs

# kwargs passes the data to the argument in the form of a dictionary
list = ["qurat", "huma", "asia", "ayesha", "The programmer\n"]
normal = "i am a normal argument and the names of students are"
kw = {"quratulain":"artist", "huma" : "writer", "asia":"genius", "ayesha":"talented", "azia":"singer"}
def func(normal, *args, **kwargs):
    print(normal)
    for names in args:
        print(names)
    print("now i would like to share my students talents")
    for key, value in kwargs.items():
        print(f"{key} is {value}")

func(normal, *list, **kw)

#  there are 2 main types of arguments
# (1) Formal argument
# (2) Actual argument
def person(name, age): # --> formal argument
    print(name)
    print(age)
person('quratulain', 22) # --> actual argument

# there are further 4-types of Actual argument
# (1) Position argument
# (2) Keywords argument
# (3) Default argument
# (4) Variable-length argument

############## position argument ############

def person(name, age):
    print(name)
    print(age)
person('quratulain', 22)
# you are passing 'quratulain' for 'name' and this is called position
#  if swap position then you will face problems
def person(name, age):
    print(name)
    print(age)
person(22, 'quratulain')

# let's subtract age by 5
def person(name, age):
    print(name)
    print(age-5)
person(22, 'quratulain') # --> error, int cannot be subtracted by str
# taking care of position is must


############## keywords argument ############

# if you only know about numbers and names of variables but not sure about position, use keyword method

def person(name, age):
    print(name)
    print(age)
person(age=22, name='quratulain') # in this taking care of position isn't must


############## default argument ############

# if you want to pass only name but not age and hoping that by default age should be 18 the...

def person(name, age=8):
    print(name)
    print(age)
person('quratulain')


############## variable-length argument ############

def sum(a, b):
    c = a + b
    print(c)
sum(5, 6)
# but sometimes we want to add more than two numbers
# for this convert b inti *b (*args)
def sum(a, *b):
    c = a + b
    print(c)
sum(5, 6, 34, 78) # --> error, because *args covert things into tuple

# let's use for-loop for tuple
def sum(a, *b):
    c = a
    for num in b:
        c = c+num
    print(c)
sum(5, 6, 34, 78)

