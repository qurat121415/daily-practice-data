# swapping first and last elements in list
# method 1
def swaplist(newlist):
    size = len(newlist)
    temp = newlist[0]
    newlist[0] = newlist[size-1]
    newlist[size-1] = temp
    return newlist
newlist = [1, 2, 3, 4]
print(swaplist(newlist))

# method 2
def swaplist(newlist):
    newlist[0], newlist[-1] = newlist[-1], newlist[0]
    return newlist
newlist = [1, 2, 3, 4]
print(swaplist(newlist))

# method 3
def swaplist(list):
    get = list[0], list[-1]
    list[-1], list[0] = get
    return list
list = [1, 2, 3,4]
print(swaplist(list))

# method 4
def swaplist(list):
    start, *middle, end = list
    list = [start, *middle, end]
    return list
list = [1, 2, 3, 4]
print(swaplist(newlist))

# method 5
def swaplist(list):
    first = list.pop(0)
    last = list.pop(-1)
    list.insert(0, last)
    list.append(first)
    return list
list = [1, 2, 3, 4]
print(swaplist(list))

# swapping elements at any position
# method 1 (temp)
def swaplist(list, pos1, pos2):
    temp = list[pos1]
    list[pos1] = list[pos2]
    list[pos2] = temp
    return list
list = [1, 2, 3, 4]
pos1 = (2)
pos2 = (3)
print(swaplist(list,pos1 - 1 ,pos2 - 1))

# method 2
def swapposi(list, pos1, pos2):
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list
list = [1, 2, 3, 4]
pos1 = (2)
pos2 = (3)
print(swapposi(list, pos1 - 1, pos2 - 1))

# method 3
def swapposi(list, pos1, pos2):
    get = list[pos1], list[pos2]
    list[pos2], list[pos1] = get
    return list
list = [1, 2, 3, 4]
pos1 = (2)
pos2 = (3)
print(swapposi(list, pos1 - 1, pos2 - 1))

# method 4
def swapPosi(list, pos1, pos2):
    first_ele = list.pop(pos1)
    sec_ele = list.pop(pos2 -1)
    list.insert(pos1, sec_ele)
    list.insert(pos2, first_ele)
    return list
list = [1, 2, 3, 4]
pos1 = (2)
pos2 = (3)
print(swapPosi(list, pos1 - 1, pos2 -1))
